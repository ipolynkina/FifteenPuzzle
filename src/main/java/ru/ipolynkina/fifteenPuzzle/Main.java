package ru.ipolynkina.fifteenPuzzle;

import ru.ipolynkina.fifteenPuzzle.frame.MainFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(MainFrame::new);
    }
}
