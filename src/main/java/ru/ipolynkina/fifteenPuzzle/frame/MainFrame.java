package ru.ipolynkina.fifteenPuzzle.frame;

import ru.ipolynkina.fifteenPuzzle.board.OldStyleBoard;
import ru.ipolynkina.fifteenPuzzle.game.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class MainFrame extends JFrame {

    public static final int PADDING_IN_PIXELS = 50;

    private static final int WIDTH_FRAME = 320;
    private static final int HEIGHT_FRAME = 300;

    private OldStyleBoard board;
    private JLabel lblMoves;
    private JLabel lblTimer;

    public MainFrame() {
        setTitle("Fifteen Puzzle");
        setSize(WIDTH_FRAME, HEIGHT_FRAME);
        setLocationRelativeTo(null);
        addWindowListener(new ExitListener());

        createInfoPanel();
        createBoard();
        createActionPanel();

        setResizable(false);
        setVisible(true);
    }

    private void createInfoPanel() {
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));

        lblMoves = new JLabel();
        infoPanel.add(lblMoves);

        lblTimer = new JLabel();
        infoPanel.add(lblTimer);
        add(infoPanel, BorderLayout.EAST);
    }

    private void createBoard() {
        board = new OldStyleBoard(lblMoves, lblTimer);
        add(board, BorderLayout.CENTER);
    }

    private void createActionPanel() {
        JPanel actionPanel = new JPanel();
        JButton btnNewGame = new JButton("Новая Игра");
        btnNewGame.addActionListener(new Game(this, board));
        actionPanel.add(btnNewGame);
        add(actionPanel, BorderLayout.PAGE_END);
    }

    private class ExitListener implements WindowListener {
        @Override public void windowClosing(WindowEvent e) {
            System.exit(0);
        }

        @Override public void windowOpened(WindowEvent e) {}
        @Override public void windowClosed(WindowEvent e) {}
        @Override public void windowIconified(WindowEvent e) {}
        @Override public void windowDeiconified(WindowEvent e) {}
        @Override public void windowActivated(WindowEvent e) {}
        @Override public void windowDeactivated(WindowEvent e) {}
    }
}
