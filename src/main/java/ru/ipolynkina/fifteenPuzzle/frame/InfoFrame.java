package ru.ipolynkina.fifteenPuzzle.frame;

import javax.swing.*;
import java.awt.*;

public class InfoFrame extends JDialog {

    public InfoFrame(JFrame owner, String title, String text) {
        super(owner, title, true);
        setSize(200, 150);
        setLocationRelativeTo(owner);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        createTextPanel(text);
        createButtonPanel();
        setResizable(false);
    }

    private void createTextPanel(String text) {
        JPanel textPanel = new JPanel();
        JLabel message = new JLabel();
        message.setText("<html>" + text + "</br></html>");
        textPanel.add(message, BorderLayout.NORTH);
        add(textPanel);
    }

    private void createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(e -> dispose());
        buttonPanel.add(btnOk, BorderLayout.PAGE_END);
        add(buttonPanel, BorderLayout.SOUTH);
    }
}
