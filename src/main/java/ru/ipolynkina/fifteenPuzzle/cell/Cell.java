package ru.ipolynkina.fifteenPuzzle.cell;

public interface Cell<T> {

    void setValue(int value);

    int getValue();

    void draw(T paint, int x, int y);
}
