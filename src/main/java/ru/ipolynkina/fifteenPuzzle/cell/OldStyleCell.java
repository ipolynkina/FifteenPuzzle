package ru.ipolynkina.fifteenPuzzle.cell;

import ru.ipolynkina.fifteenPuzzle.frame.MainFrame;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class OldStyleCell implements Cell<Graphics> {

    private static final int AMOUNT_OF_PUZZLES = 16;
    private static BufferedImage[] imgPuzzles = new BufferedImage[AMOUNT_OF_PUZZLES];

    private int value;

    static {
        try {
            for(int i = 0; i < imgPuzzles.length; i++) {
                imgPuzzles[i] = ImageIO.read(Objects.requireNonNull(Thread.currentThread()
                        .getContextClassLoader()
                        .getResourceAsStream("img/" + i + ".jpg")));
            }
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    public OldStyleCell() {
        value = 0;
    }

    @Override
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public void draw(Graphics paint, int x, int y) {
        paint.drawImage(imgPuzzles[value],
                x * MainFrame.PADDING_IN_PIXELS,
                y * MainFrame.PADDING_IN_PIXELS,
                null);
    }
}
