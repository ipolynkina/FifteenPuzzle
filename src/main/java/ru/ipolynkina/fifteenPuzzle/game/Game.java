package ru.ipolynkina.fifteenPuzzle.game;

import ru.ipolynkina.fifteenPuzzle.board.OldStyleBoard;
import ru.ipolynkina.fifteenPuzzle.frame.InfoFrame;
import ru.ipolynkina.fifteenPuzzle.frame.MainFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Game implements ActionListener, MouseListener {

    private JFrame ownerFrame;
    private OldStyleBoard board;

    private int x;
    private int y;

    public Game(JFrame ownerFrame, OldStyleBoard board) {
        this.ownerFrame = ownerFrame;
        this.board = board;
        this.board.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        board.startGame();
    }

    private void movePuzzle() {
        if(!board.puzzleIsCorrect()) {
            board.movePuzzles(x, y, true);
            if(board.puzzleIsCorrect()) {
                InfoFrame infoFrame = new InfoFrame(ownerFrame, "Win",
                        "<html><br>Поздравляем,<br>Пазл собран!</html>");
                infoFrame.setVisible(true);
            }
        }
    }

    private void saveCoordinates(MouseEvent event) {
        x = event.getX() / MainFrame.PADDING_IN_PIXELS;
        y = event.getY() / MainFrame.PADDING_IN_PIXELS;
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        saveCoordinates(event);
        movePuzzle();
    }

    @Override
    public void mousePressed(MouseEvent event) {
        saveCoordinates(event);
        movePuzzle();
    }

    @Override public void mouseReleased(MouseEvent event) {}
    @Override public void mouseEntered(MouseEvent event) {}
    @Override public void mouseExited(MouseEvent event) {}
}
