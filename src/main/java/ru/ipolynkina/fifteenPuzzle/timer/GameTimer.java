package ru.ipolynkina.fifteenPuzzle.timer;

import javax.swing.*;
import java.util.concurrent.TimeUnit;

public class GameTimer implements Runnable {

    private JLabel lblTimer;
    private Integer timeInSeconds = 0;

    public GameTimer(JLabel lblTimer) {
        this.lblTimer = lblTimer;
        lblTimer.setText(getTimeInSeconds());
    }

    @Override
    public void run() {
        try {
            while(!Thread.interrupted()) {
                TimeUnit.SECONDS.sleep(1);
                timeInSeconds++;
                lblTimer.setText(getTimeInSeconds());
            }
        } catch(InterruptedException exc) {
            exc.printStackTrace();
        }
    }

    public void reset() {
        timeInSeconds = 0;
        lblTimer.setText(getTimeInSeconds());
    }

    private String getTimeInSeconds() {
        return "<html><pre>Секунд: " +  timeInSeconds.toString() + "  </pre></html>";
    }
}
