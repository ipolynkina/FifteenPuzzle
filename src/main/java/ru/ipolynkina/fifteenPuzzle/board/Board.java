package ru.ipolynkina.fifteenPuzzle.board;

public interface Board {

    void startGame();

    void movePuzzles(int x, int y, boolean isGamer);

    boolean puzzleIsCorrect();
}
