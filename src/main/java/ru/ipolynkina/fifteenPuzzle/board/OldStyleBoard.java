package ru.ipolynkina.fifteenPuzzle.board;

import ru.ipolynkina.fifteenPuzzle.cell.Cell;
import ru.ipolynkina.fifteenPuzzle.cell.OldStyleCell;
import ru.ipolynkina.fifteenPuzzle.frame.MainFrame;
import ru.ipolynkina.fifteenPuzzle.timer.GameTimer;

import javax.swing.*;
import java.awt.*;

public class OldStyleBoard extends JPanel implements Board {

    private static final int SIZE_X = 4;
    private static final int SIZE_Y = 4;
    private Cell[][] cells = new OldStyleCell[SIZE_X][SIZE_Y];

    private final int[][] correctPuzzle = {
            { 1, 5,  9, 13 },
            { 2, 6, 10, 14 },
            { 3, 7, 11, 15 },
            { 4, 8, 12,  0 }
    };

    private int emptyCellX;
    private int emptyCellY;
    private boolean gameIsStarted = false;

    private JLabel lblMoves;
    private int amountOfMoves = 0;

    private GameTimer gameTimer;
    private Thread thread;

    public OldStyleBoard(JLabel lblMoves, JLabel lblTimer) {
        this.lblMoves = lblMoves;
        gameTimer = new GameTimer(lblTimer);
        startGame();
    }

    @Override
    public void startGame() {
        if(gameIsStarted) stopCounters();

        gameTimer.reset();
        amountOfMoves = 0;

        createCorrectBoard();
        mixPuzzles();
    }

    private void startCounters() {
        gameIsStarted = true;
        thread = new Thread(gameTimer);
        thread.start();
    }

    private void stopCounters() {
        gameIsStarted = false;
        thread.interrupt();
    }

    private void createCorrectBoard() {
        for(int x = 0; x < SIZE_X; x++) {
            for(int y = 0; y < SIZE_Y; y++) {
                cells[x][y] = new OldStyleCell();
                cells[x][y].setValue(correctPuzzle[x][y]);
            }
        }

        emptyCellX = SIZE_X - 1;
        emptyCellY = SIZE_Y - 1;
    }

    private void mixPuzzles() {
        final int AMOUNT_OF_MIX = 50;
        for(int i = 0; i < AMOUNT_OF_MIX; i++) {
            int x = (int) (Math.random() * SIZE_X);
            int y = (int) (Math.random() * SIZE_Y);
            movePuzzles(emptyCellX, y, false);
            movePuzzles(x, emptyCellY, false);
        }
    }

    @Override
    public void movePuzzles(int x, int y, boolean isGamer) {
        if(isGamer && !gameIsStarted) startCounters();

        if(x >= 0 && x < SIZE_X && y >= 0 && y < SIZE_Y) {
            if(x == emptyCellX) {
                if(emptyCellY - y > 0) moveToDown(x, y, isGamer);
                else moveToUp(x, y, isGamer);
            }

            else if(y == emptyCellY) {
                if(emptyCellX - x < 0) moveToRight(x, y, isGamer);
                else moveToLeft(x, y, isGamer);
            }
        }

        lblMoves.setText("<html><pre>Ходов:  " + amountOfMoves + "  </pre></html>");
        repaint();

        if(isGamer && puzzleIsCorrect()) stopCounters();
    }

    private void moveToRight(int x, int y, boolean isGamer) {
        if(isGamer && emptyCellX != x) amountOfMoves++;

        for(int startX = emptyCellX; startX < x; startX++) {
            int temp = cells[startX][y].getValue();
            cells[startX][y].setValue(cells[startX + 1][y].getValue());
            cells[startX + 1][y].setValue(temp);
            emptyCellX = x;
        }
    }

    private void moveToLeft(int x, int y, boolean isGamer) {
        if(isGamer && emptyCellX != x) amountOfMoves++;

        for(int startX = emptyCellX; startX > x; startX--) {
            int temp = cells[startX][y].getValue();
            cells[startX][y].setValue(cells[startX - 1][y].getValue());
            cells[startX - 1][y].setValue(temp);
            emptyCellX = x;
        }
    }

    private void moveToUp(int x, int y, boolean isGamer) {
        if(isGamer && emptyCellY != y) amountOfMoves++;

        for(int startY = emptyCellY; startY < y; startY++) {
            int temp = cells[x][startY].getValue();
            cells[x][startY].setValue(cells[x][startY + 1].getValue());
            cells[x][startY + 1].setValue(temp);
            emptyCellY = y;
        }
    }

    private void moveToDown(int x, int y, boolean isGamer) {
        if(isGamer && emptyCellY != y) amountOfMoves++;

        for(int startY = emptyCellY; startY > y; startY--) {
            int temp = cells[x][startY].getValue();
            cells[x][startY].setValue(cells[x][startY - 1].getValue());
            cells[x][startY - 1].setValue(temp);
            emptyCellY = y;
        }
    }

    @Override
    public boolean puzzleIsCorrect() {
        for(int x = 0; x < SIZE_X; x++) {
            for(int y = 0; y < SIZE_Y; y++) {
                if(cells[x][y].getValue() != correctPuzzle[x][y]) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        for(int x = 0; x < SIZE_X; x++) {
            for(int y = 0; y < SIZE_Y; y++) {
                graphics.setColor(Color.BLACK);
                cells[x][y].draw(graphics, x , y);
                graphics.drawRect(x * MainFrame.PADDING_IN_PIXELS, y * MainFrame.PADDING_IN_PIXELS,
                        MainFrame.PADDING_IN_PIXELS, MainFrame.PADDING_IN_PIXELS);
            }
        }
    }

    public int getValue(int x, int y) {
        return cells[x][y].getValue();
    }

    public int getEmptyCellX() {
        return emptyCellX;
    }

    public int getEmptyCellY() {
        return emptyCellY;
    }
}
