package ru.ipolynkina.fifteenPuzzle;


import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.RepeatedTest;
import ru.ipolynkina.fifteenPuzzle.board.OldStyleBoard;

import javax.swing.*;

public class BoardTest {

    private static final int SIZE_X = 4;
    private static final int SIZE_Y = 4;

    private static JLabel lblMoves = new JLabel();
    private static JLabel lblTimer = new JLabel();

    private static OldStyleBoard board;
    private static int[] controlLineX = new int[SIZE_X];
    private static int[] controlLineY = new int[SIZE_Y];

    @BeforeClass
    public static void init() {
        board = new OldStyleBoard(lblMoves, lblTimer);
    }

    @Test
    public void moveHorizontallyTest() {
        int randomX = (int) (Math.random() * SIZE_X);
        int emptyX = board.getEmptyCellX();
        int emptyY = board.getEmptyCellY();

        for(int i = 0; i < SIZE_X; i++) {
            controlLineX[i] = board.getValue(i, emptyY);
        }

        board.movePuzzles(randomX, emptyY, false);

        if(emptyX - randomX < 0) {
            for(int startX = emptyX; startX < randomX; startX++) {
                int temp = controlLineX[startX];
                controlLineX[startX] = controlLineX[startX + 1];
                controlLineX[startX + 1] = temp;
            }
        } else if(emptyX - randomX > 0) {
            for(int startX = emptyX; startX > randomX; startX--) {
                int temp = controlLineX[startX];
                controlLineX[startX] = controlLineX[startX - 1];
                controlLineX[startX - 1] = temp;
            }
        }

        for(int i = 0; i < SIZE_X; i++) {
            System.out.println(controlLineX[i] + " " + board.getValue(i, emptyY));
            Assert.assertEquals(controlLineX[i], board.getValue(i, emptyY));
        }
        System.out.println();
    }

    @Test
    public void moveVerticallyTest() {
        int emptyX = board.getEmptyCellX();
        int emptyY = board.getEmptyCellY();
        int randomY = (int) (Math.random() * SIZE_Y);

        for(int i = 0; i < SIZE_Y; i++) {
            controlLineY[i] = board.getValue(emptyX, i);
        }

        board.movePuzzles(emptyX, randomY, false);

        if(emptyY - randomY < 0) {
            for(int startY = emptyY; startY < randomY; startY++) {
                int temp = controlLineY[startY];
                controlLineY[startY] = controlLineY[startY + 1];
                controlLineY[startY + 1] = temp;
            }
        } else if(emptyY - randomY > 0) {
            for(int startY = emptyY; startY > randomY; startY--) {
                int temp = controlLineY[startY];
                controlLineY[startY] = controlLineY[startY - 1];
                controlLineY[startY - 1] = temp;
            }
        }

        for(int i = 0; i < SIZE_Y; i++) {
            System.out.println(controlLineY[i] + " " + board.getValue(emptyX, i));
            Assert.assertEquals(controlLineY[i], board.getValue(emptyX, i));
        }
        System.out.println();
    }

    @RepeatedTest(10)
    public void testRepeat() {
        moveHorizontallyTest();
        moveVerticallyTest();
    }
}
